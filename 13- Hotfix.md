# Hotfix

### Hotfix, o telefonema no meio da madrugada.

Para simplificar, considere um hotfix, ou mesmo um conjunto de hotfixes, um pacote usado para corrigir um série de bugs(falhas), seja em aplicativos ou no próprio sistema.
Como descrito nos conceitos Gitflow, esta branch é a única que sai do Master. ela corrige bugs que emergenciais e depois vai para master novamente e develop. Vamos trabalhar novamente na linha de comando.

Muitas alterações já foram feitas no repositório remoto e seu repositório local (caso não tenha deletado ele) estará bem atrasados com as mudanças que fizemos na interface gráfica.   
Para atualizar executamos o comando:
```
git checkout master
git pull
```

Criamos a nova branch de hotfix:
```
git checkout -b hotfix/1.0.1
```

O que queremos mudar é o arquivo index.html substituindo a mensagem do rodapé:
de: "Mudamos a branch feature/novo-rodape"
para: "Versionamos corretamente corrigido hotfix"

Pronto, problema resolvido, agora efetuamos os stages para o versionamento.
```
git add *
git commit -m "Ajustado a mensagem de rodape"
git push
```

Feito isto, vamos fazer merge do hotfix para Master.
```
git checkout master
git merge hotfix/1.0.1
git push
git checkout develop
git merge hotfix/1.0.1
git push
```