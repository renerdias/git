# O Desafio
![](cadecode.jpeg)

# Migração
![](upcode.jpg)

# Conclusão
![](okcode.jpg)

>>>
# Importância de um versionador
Na medida em que projetos de software estam em uma infra estrutura e tornam-se mais complexos, aumenta-se a demanda por respostas rápidas a mudanças
e a necessidade de integração entre sistemas. 
Sistema de controle de versão deixa de ser uma tendência tecnológica para se tonar componente essenciail. 
Mais do que um número para acompanhar o desenvolvimento, essa deve ser uma prática que garante mais segurança para o código e para a operação da TI dentro de uma empresa.


# Práticas do código

- Fácil : Quando tem algo mais rápido, acessível e mais simples, tem um resultado mais rápido.
- Numerar o código: Versionamento
- Histórico : Mudanças quem alterou e o que foi alterado
- Recuperação : Corrigir rapidamente ou utilizar versões anteriores quando necessário
- Trabalho colaborativo : Integração entre a equipe de desenvolvimento
- Análise de ameaças : Rastrear no código possíveis ameaças

>>>

# Quem utiliza GitLab
>>>
GitLab é utilizado por mais de 100.000 organizações em todo o mundo.
### Tecnologia
- Capgemini - Uma das maiores fornecedores de serviços de consultoria, tecnologia e outsourcing do mundo
- Red Hat - disponibiliza soluções baseadas no sistema operacional GNU/Linux, além de soluções de software
- Sony - quinto maior conglomerado de mídia do planeta.

### Ciência e pesquisa
- Bayer - Empresa global com competências em Ciências da Vida nas áreas de agricultura e cuidados com a saúde humana e animal. 
- KWS Saat - o quinto maior produtor mundial de sementes com base em vendas.

### Setor Público
- Nasa - Responsável pela pesquisa e desenvolvimento de tecnologias e programas de exploração espacial.
- Interpol - A Organização Internacional de Polícia Criminal

>>>


# Diferenças entre SVN e GIT

>>>
- Exceto o pull inicial, todas as outras tarefas são exponencialmente muito mais rápidos do que sistemas centralizados como CVS e SVN.
- Praticamente todas as operações não necessitam de acesso ao repositório, então o desenvolvedor pode trabalhar offline, sincronizando com o repositório apenas quando necessário.
- Git é SCM, gerenciamento de código fonte e um sistema de controle de revisão distribuído. O SVN é um sistema de controle de revisão e de versão de software .
- Renomear diretórios é bastante complicado no SVN, no GIT com um comando ou cliques isto pode ser feito.
- No GIT criar uma branch (ramificação) é mais simples e mais facil de gerir, no SVN deve ser criada outra pasta no repo e é bem mais complicado.
- No SVN é facíl criar novos branchs, mas juntá-los novamente não.
- No GIT ao criar um repositório pode ser escolhido entre: 
  - **Privado**: O acesso do projeto deve ser concedido explicitamente para cada usuário.
  - **Interno**: O projeto pode ser acessado por qualquer usuário logado.
  - **Público**: O projeto pode ser acessado sem qualquer autenticação.
- No Git podem ser geradas tags, branches e versões que serão simplesmente descartadas.
- Git possui mais informações de auditoria e que permitem mais facilidades em toda a administração.


Quando você cria um branch no Git, ele faz de modo local e isso acontece de maneira bem rápida. Aqui está um exemplo da criação de um branch 
e então a troca para um novo branch para começar a fazer o desenvolvimento.
```bash
$ time git branch myidea
real 0m0.009s
user 0m0.002s
sys  0m0.005s

$ time git checkout myidea
Switched to branch "myidea"
real 0m0.298s
user 0m0.004s
sys  0m0.017s
```
Levou cerca de um terço de segundo para ambos os comandos serem executados juntos. Pense por um segundo sobre o equivalente no Subversion – executar um `copy` e então um `switch`.
```bash
$ time svn copy -m 'my idea'  \

real 0m5.172s
user 0m0.033s
sys 0m0.016s

$ time svn switch
real 0m8.404s
user 0m0.153s
sys 0m0.835s
```

>>>

# Melhorias do GitLab

>>>
- wiki
- gestão de issues
- tickets
- cambam
- releases
- tags


>>>

# Considerações

>>>

### Pontos cruciais

A mudança pode ser significativa para equipes de desenvilvimento, mas esse "calo" faz parte da evolução e organização de códigos.

### migração de SNV para GIT
usar a ferramenta para migrar o SVN para Git com a ferramenta svn2git.

>>>

* Faça o teu melhor, na condição que você tem, enquanto você não tem condições melhores, para fazer melhor ainda...
*Mario Sergio Cortella*

# Fontes:
- https://www.ibm.com/developerworks/community/blogs/fd26864d-cb41-49cf-b719-d89c6b072893/entry/por_que_voc_C3_AA_deveria_mudar_do_subversion_para_git_parte_01?lang=en
- https://docs.gitlab.com/ee/README.html
- https://about.gitlab.com/customers/

