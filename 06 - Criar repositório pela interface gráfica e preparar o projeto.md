### Criando um repositório pela interface gráfica do GitLab.

Você já pode ir direto pela URL caso desejar:    
[https://gitlab.com/projects/new](https://gitlab.com/projects/new)

Caso não utilizar a URL acima, você pode utilizar o seguinte caminho:
[https://gitlab.com/](https://gitlab.com/)
Clicar no botão **Novo projeto**

Agora você têm inúmeras possibilidades de criar o seu repositório. Pode criar em branco, Criar à partir de um template de .NET, NodeJS, Ruby. Pode importar um projeto de outro repositório como GitHub, Bitbucket e outros. No nosso caso, vamos criar um Projeto em branco.
**Nome:** repotest   
**URL do projeto:** Aqui você têm a possibilidade de deixa-lo em seu nome ou adicionar a um grupo que existe. Neste caso, deixe com o nome de seu usuário.   
**Slug do projeto:** Pode adicionar uma URL amigável caso desejar.   
Adicione um comentário opcionalmente.    
Escolha a visibilidade de seu projeto (Público ou privado).

Clique logo em seguida no botão **Criar projeto**   
![](imagens/repo-g01.png)

O Git, é muito interativo. Ao acessar o repositório recém criado, ele nos apresenta várias possibilidades de enviar seu código.   
Para não ficarmos espantados, vamos pela etapa mais simples que é a **Push an existing folder**.   

Baixe o nosso projeto piloto através da URL:   
[https://gitlab.com/treinamentos1/treinamento-git/-/archive/master/treinamento-git-master.zip](https://gitlab.com/treinamentos1/treinamento-git/-/archive/master/treinamento-git-master.zip)

Após baixar este projeto, extraia-o.   
Você agora pode escolher em como clonar seu repositório remoto, se você criou sua chave ssh utilize a opção de Clonar com SSH, se não, então vai de HTTPS mesmo:   
![](imagens/repo-g02.png)


```
git clone git@gitlab.com:claudiosilva/repotest.git
```

Agora você têm um snapshot do seu repositório localmente. Copie os arquivos do treinamento-git-master.zip para dentro deste diretório.   
Após finalizar a cópia, há um comando que pode ver o status do que foi adicionado e está por subir ao repositório remoto, execute o comando **git status**:
```
git status                                        
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	README.md
	images/
	index.html
	styles/

nothing added to commit but untracked files present (use "git add" to track)
```

A saída do comando acima, descreve que você está na branch master, nenhum commit foi feito, nenhum arquivo ainda está sendo rastreado, mas há novos arquivos que podem ser rastrados (fica em vermelho geralmente).


Agora vamos ao segundo processo que é executar o `git add` Este comando atualiza o índice usando o conteúdo atual dentro do diretório, preparando o conteúdo para a próxima confirmação.
```
git add *
```

Se você verificar o status novamente verá que os arquivos agora estão sendo rastrados.
```
git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

	new file:   README.md
	new file:   images/gitlab_logo.png
	new file:   index.html
	new file:   styles/style.css

```

Agora, vamos efetuar um commit. O commit registra alterações no repositório, ele confirma o conteúdo atual do índice e a mensagem de log fornecida que descreve as alterações.
```
git commit -m "Nosso primeiro envio dos dados."
[master (root-commit) 4ba8f1a] Nosso primeiro envio dos dados.
 4 files changed, 96 insertions(+)
 create mode 100644 README.md
 create mode 100644 images/gitlab_logo.png
 create mode 100644 index.html
 create mode 100644 styles/style.css
```

Ainda não foi enviado seus dados para o repositório remoto, agora é a hora executando o comando push.
```
git push
```

Pronto, agora seu repositório remoto está com todo o conteúdo.
![](imagens/repo-g03.png)