# SCM e Versionamento de Código efetivo: Git, Gitlab e Gitflow

![](https://wac-cdn.atlassian.com/dam/jcr:8e57216e-269e-49e6-aff2-5c03b8512e73/hero.svg?cdnVersion=1072)

## Não Reinvente a Roda
#### 1 Patterns, Linguagem Comum
A maneira mais efetiva de iniciar um trabalho, que envolve muitas pessoas, diversas equipes, é definir uma liguagem comum, falar o mesmo protocolo sempre fez, e provavelmente fará parte de nossa evolução tecnológica.Devido a isto, são criados os Padrões trazendo melhor qualidade e produtividade, aos times envolvidos.

# Git, GitLab, GitHub, Gitflow, entenda o que são!
####  Introdução
Vamos, antes de mais nada, entender o que quer dizer cada sigla, ou nome desses:
**SCM:** É a abreviação de source code management, em tradução, Sistema de Controle de Versão. O SCM rastreia um histórico de alterações em uma base de código e ajuda a resolver conflitos ao mesclar atualizações de vários colaboradores, existem várias ferramentas com a finalidade de SCM como SVN e Git.   

**Git:** é a nossa ferramenta de SCM distribuído, pode-se dizer que é uma evolução dos modelos centralizados como o SVN e CVS.   

**Git -hub -lab:** são plataformas desenvolvidas para operar sob o GIT, num modo bem simplista, são interfaces que facilitam a utilização (além de adicionar outras funcionalidades) ao GIT.   

**Gitflow:** este nome tem o prefixo 'git' por mera 'coincidência' (ou jogada) do autor, se trata de um framework de versionamento, e não está ligado de nenhuma forma ao git.

# Gitflow, nosso maestro

#### Prazer, Gitlflow
Como foi dito anteriormente, Gitflow é um modelo com um conjunto muito simples de regras, que orquestram desenvolvimentos paralelos.
Antes de iniciarmos, entendam que, este modelo pode ser aplicado em qualquer ferramenta de SCM!
Estamos falando simplesmente de um modelo organizacional lógico!


#### Estruturas de Branch

Vamos direto ao ponto, iremos conhecer os tipos de branches:

##### master

**Descrição:** Em alguns locais é chamada de **Producao** Esta branch é a mais importante, reflete nosso código produtivo!   
**Tipo:** Principal   
**Tempo de Vida:** Infinito   
**Criada de:** N/A   
**Pode ir Para:** develop   

##### develop

**Descrição:** Em vários lugares pode ser encontrada aí como **homologacao**. Contém os códigos do próximo release, também conhecido como branch de integração.
Quando a branch develop estiver pronta para entrar em produção, um 'merge' pode ser feito para a branch master.

**Tipo:** Principal   
**Tempo de Vida:** Infinito   
**Criada de:** master (uma única vez)   
**Pode ir Para:** master   

### feature/*

**Descrição:** Esta linha é criada para desenvolver uma nova funcionalidade, e ela só deverá existir nesse espaço de tempo.
Uma feature branch pode também ter seu desenvolvimento cancelado por N motivos.

**Tipo:** Suporte   
**Tempo de Vida:** Finito   
**Criada de:** develop   
**Pode ir Para:** develop   

### release-*

**Descrição:** É praticamente **pré produção**, viabiliza ajustes finos e prepara os metadados (version numbers, notes, ...), e libera a linha de develop para receber conteúdos do próximo release.

**Tipo:** Suporte   
**Tempo de Vida:** Finito   
**Criada de:** develop   
**Pode ir Para:** develop & master   

### hotfix
**Descrição:** Correção de erros identificados em ambiente de produção.

**Tipo:** Suporte   
**Tempo de Vida:** Finito   
**Criada de:** master   
**Pode ir Para:** master & develop   

### Tag

**Descrição:** Etiqueta que demarca um ponto (commit) que representa uma versão efetiva para produção do projeto.
**Tipo:** Principal   
**Tempo de Vida:** infinito   
**Criada de:** master   

### Versionamento Semântico
Vamos conhecer agora o pattern mais utilizado, em todas comunidades e times, sejam eles open-source ou não:
![](semver.png)   

Semantic Versioning : https://semver.org/lang/pt-BR/
