# Removendo repositório.

A remoção de um diretório é simples, para agilizar nosso processo, vamos fazer pela interface. Primeiro vamos remover o repositório **testmanual** criado anteriormente:   
Grupos -> Seus grupos -> test-treinamento.   
![](imagens/rmgrp-01.png)

Cliquem no repositório desejado:
![](imagens/rmgrp-02.png)

Na barra lateral, vá até a opção de configurações -> Geral -> Avançado Remover projeto e clique no botão **Remover projeto**   
![](imagens/rmgrp-03.png)

Confirme a exclusão adicionando o nome do repositório e clique em **Confirmar**    
![](imagens/rmgrp-04.png)

# Removendo Grupo

Vamos remover o grupo criado anteriormente, vá até **Grupos** -> **Seus grupos** -> **test-treinamento**.   
![](imagens/rmgrp-05.png)

Na barra lateral, vá até a opção de configurações -> Geral -> Avançado Remover projeto e clique no botão **Remover grupo**.   
![](imagens/rmgrp-06.png)

Confirme a exclusão do grupo junto com todo o seu conteúdo.   
![](imagens/rmgrp-07.png)