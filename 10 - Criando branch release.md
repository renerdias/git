# Criando branch release pela interface gráfica
Vamos criar uma nova branch de release.   
de: **develop**    
para: **release**

Na página inicial do seu repositório, clique em branches -> Nova branch.   
Em Branch name  **release-1.0.1**   
Em Create from **develop**   
Clicamos em **Create branch**