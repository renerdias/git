# Criando tags
Tags são etiquetas que demarcam um ponto (commit) que representa alguma mudança significativa no seu código, ou seja, uma versão (ou release) do seu projeto.

Como a maioria dos versionadores de código, o Git tem a capacidade de marcar pontos específicos no histórico de um repositório como sendo importantes. Nesta seção, você aprenderá como listar tags existentes, como criar e excluir tags e quais são os diferentes tipos de tags.

```
git tag -a 1.0.0 -m "Primeira versão do nosso código estável"
```

Listando tags:
```
git tag
```

Por padrão, o git pushcomando não transfere tags para servidores remotos. Você precisará enviar explicitamente as tags para um servidor compartilhado depois de criá-las.
```
git push origin 1.0.0
```

Você também pode procurar por tags que correspondam a um padrão específico.
```
git tag -l "1.*"
```

Para finalizar, se você desejar remover uma tag:
```
git tag -d 1.0.0
git push origin --delete 1.0.0
```