# Grupos e subgrupos

#### Grupos
Com os grupos GitLab, você pode:
* Montar projetos relacionados juntos.
* Conceda acesso aos membros em vários projetos ao mesmo tempo, ou apenas em um específico.
* Grupos podem ser aninhados em subgrupos.
* Facilitar a vida de toda a sua equipe de uma só vez criando um grupo e incluindo os membros apropriados. Por exemplo, você pode criar um grupo para os membros da sua empresa e criar um subgrupo para cada equipe individual.

### Criando um grupo:

Vá em: Grupos -> Seus grupos.   
Nesta localização apresenta grupos relacionados a você. também pode ser criado novos grupos pelo botão **Novo grupo**.   
Caso preferir criar diretório por uma URL é só acessar [https://gitlab.com/groups/new](https://gitlab.com/groups/new)

Adicione o que se peder:
* o Nome do grupo
* Descrição
* Escolha um Avatar (Imagem que representará o grupo)
* Escolha se o grupo será privado para ninguêm ver o conteúdo, dando permissões gradativamente ou público que todos possam ver o conteúdo.
![](imagens/grp-01.png)

Após as etapas acima, seu grupo estará criado com sucesso.   
![](imagens/grp-02.png)


#### Subgrupos
O GitLab suporta até 20 níveis de subgrupos. Na prática podemos explicar grupos e subgrupos da seguinte forma com o exemplo abaixo:

- Linux
  - Java
    - App1
    - App2
  - PHP
    - App1
- Windows
  - ASP
    - App1
    - App2

Para criar um subgrupo:

1. Dentro do grupo desejado, expanda o botão Novo projeto, selecione Novo subgrupo e clique no botão Novo subgrupo.   
![](imagens/grp-03.png)

2. Crie um novo grupo como faria normalmente. Observe que o espaço para nome do grupo pai é corrigido no caminho do grupo . O nível de visibilidade pode diferir do grupo pai.   
![](imagens/grp-04.png)

3. Clique no botão Criar grupo e você será direcionado para a página do painel do novo sub-grupo.

Siga o mesmo processo para criar quaisquer grupos subsequentes.

#### Namespaces

No GitLab, um espaço para nome é um nome exclusivo a ser usado como nome de usuário, nome de grupo ou nome de subgrupo.

* http://gitlab.com/username
* http://gitlab.com/groupname
* http://gitlab.com/groupname/subgroup_name