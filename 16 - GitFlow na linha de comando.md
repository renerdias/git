# Introdução
O Gitflow é apenas uma ideia abstrata do fluxo de trabalho Git. Isto significa que ele dita que tipos de ramificações configurar e como fazer a mesclagem.

Nunca deve ser desenvolvido diretamente nas branches principais Develop e Master, deve ser feito apenas merge. As ramificações geralmente são criadas à partir destas duas branches principais.

## Clone repositório
Ao criar o repositório é criado também a branch master.
Baixar o repositório:
git clone $REPOSITORIO

## Iniciar o GitFlow

Executar o comando abaixo no repositório local vai organizar uma sequência de branches padrões:
```
git flow init
```

Criamos a hierarquia de branches do Gitflow, e vai cair na branch master.

#### Novo artefato
Envie o artefato .gitlab-ci.yml para testar execução das pipelines. 


Adicione ao rastramento do git
```
git add .
git commit -m "Teste"
git push

```

#### Teste Pipeline
Faça um teste na pipeline


Para fazer merge com a master, basta executar o comando padrão do GIT
```
git checkout master
git merge develop
```

## Push na master

Efetuar merge de develop para master e efetuar push
```
git checkout master
git merge develop
git push
```

#### Teste Pipeline
Faça um teste na pipeline


## Criação de branch feature

```
git flow feature start ca12345
```

Envie para o repositório remoto para testar a Pipeline.
```
git push
```

#### Teste Pipeline
Faça um teste na pipeline

#### Finalização da branch feature
Quando você concluir o trabalho de desenvolvimento na branch, a próxima etapa é mesclar a ramificação de feature na de develop.

```
git flow feature finish
```

Faça push em Develop para o repositório remoto contendo alterações.
```
git push
```


## Branch de release
Uma vez que a branch **develop** adquiriu recursos o bastante para um lançamento (ou uma data de lançamento predeterminada está se aproximando), você criar uma branch **release** a partir de develop. Criar esta ramificação dá início ao próximo ciclo de lançamento, portanto nenhum novo recurso pode ser adicionado depois deste ponto—apenas correções de bug. Quando estiver pronta para ser lançada, a ramificação de lançamento é mesclada com a branch principal e marcada com uma tag com um número de versão. Além disso, ela deve ser mesclada de volta com a ramificação develop caso for efetuado alguma alteração de bug.

```
git flow release start 0.1.0
```

Enviar a branch release para teste na Pipeline
```
git push
```

#### Teste Pipeline
Faça um teste na pipeline

#### finalizar a branch de release
Ao finalizar a branch release, ela será automaticamente removida e efetuada a mesclagem LOCALMENTE para branch Master e Develop, também é criada uma TAG.

```
git flow release finish
```

Adicine um comentário do que foi alterado...

Faça push em Develop, efetue a homologação.

```
git checkout develop
git push
```

Envie para Master e logo em seguida envie também a tag para o deploy em produção.

```
git checkout master
git push

git push origin 0.1.0
```

#### Teste Pipeline
Teste a TAG na pipeline


## Branches de hotfix

As branches de manutenção ou “hotfix” são usadas para corrigir com rapidez lançamentos de produção. As branches de hotfix se parecem com releases e e features, com a diferença de serem baseadas a partir da branch master em vez de develop. 

Uma branch de hotfix pode ser criada usando o seguinte método:

```
git flow hotfix start 0.1.1
```

Envie a Hotfix para testar a pipeline
```
git push
```

#### Teste Pipeline
Teste a pipeline


#### Finalizar a branch hotfix

Assim que a correção é concluída nesta branch, ela automaticamente será mesclada na branch **master**, **develop** e cria uma **tag**.

```
git flow hotfix finish
```

Após mesclagem do Hotfix foi feita para develop, faça um push no repositório remoto e faça a homologação.
```
git checkout develop
git push 
```

Em seguida faça um push para master.
```
git checkout master
git push
```

Envie a tag para deploy em produção.
```
git push origin 0.1.1
```

# Resumo
Aqui, é discutido o Gitflow Workflow. Um dos muitos estilos de fluxos de trabalho Git que a equipe podem utilizar.
 
O fluxo geral do Gitflow é:

- Uma branch de develop é criada a partir da branch master.
- Uma branch de release é criada a partir da branch develop.
- branches de release são criadas a partir de branch develop.
- Quando uma feature é concluída, ele é mesclado na branch develop. 
- Quando a branch de release é concluída, ela é mesclada nas branches de develop e master.
- Caso um problema seja detectado na branch master,uma branch de hotfix é criada a partir da master.
- Após a conclusão da branch de hotfix, ela é mesclada para as branches de develop e master.


Fontes:

https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow

https://fjorgemota.com/git-flow-uma-forma-legal-de-organizar-repositorios-git/