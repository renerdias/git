### Merge Feature para Develop

Merge (Mesclagem) É exatamente como o nome indica: uma mesclagem de uma branch em outra, vamos fazer uma mesclagem entre duas branches
de: **feature/novo-rodape**
para: **develop**

Acessamos a branch develop:
```
git checkout develop
```

Solicitamos o merge com o seguinte comando:
```
git merge feature/novo-rodape
```

Após o merge enviamos o código para o repositório remoto.
```
git push
```

Como a branch feature têm tempo finito de existência e já finalizamos a alteração da funcionalidade, podemos remover ela.   
Removeremos a branch feature localmente e remotamente com o seguinte comando:
```
git branch -d feature/novo-rodape
git push origin -d feature/novo-rodape
```

### Nova feature.

Agora, vamos trabalhar em uma nova feature, suponhamos que queremos mudar a cor do rodapé, criaremos então uma nova branch.
```
git checkout -b feature/rodape-verde develop
```

Dentro do diretório, no arquivo **index.html** mudaremos a linha do footer 
de:<footer class="footer">
para: <footer class="footer" style="background-color: #006699;">

Mudamos também a mensagem do rodapé:
de: Mudamos a branch feature/novo-rodape
para: Nosso rodapé agora é azul.

Salve o arquivo e vamos adicionar o arquivo modificado, efetuar o commmit e enviar a nova branch ao repositório remoto.
```
git add *
git commit -m "Nova cor do rodape"
git push
```
NOTA: Como a nova branch não está declarada no git ele solicitará que declare o upstream antes do push.