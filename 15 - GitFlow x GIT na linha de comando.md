# Introdução
O Gitflow é apenas uma ideia abstrata do fluxo de trabalho Git. Isto significa que ele dita que tipos de ramificações configurar e como fazer a mesclagem.

#### Como funciona?
![](https://wac-cdn.atlassian.com/dam/jcr:b5259cce-6245-49f2-b89b-9871f9ee3fa4/03%20(2).svg?cdnVersion=1109)

Nunca deve ser desenvolvido diretamente nas branches principais Develop e Master, deve ser feito apenas merge. As ramificações geralmente são criadas à partir destas duas branches principais.

## Clone repositório
Ao criar o repositório é criado também a branch master.
Baixar o repositório:
git clone $REPOSITORIO

## Branch develop
Agora é complementar a branch master padrão com uma ramificação de desenvolvimento. Um jeito simples de alcançar isto é criando uma branch develop local e fazendo o push para o server:
```
git branch develop
git push -u origin develop
```

Executar o comando abaixo no repositório local vai organizar uma sequência de branches padrões:
```
git flow init
```

Para fazer merge com master, basta executar o comando padrão do GIT
```
git checkout master
git merge develop
```

## Criação de branch feature
Sem as extensões do git-flow:
```
git checkout develop
git checkout -b feature/ca12345
```
Ao usar a extensão do git-flow:
```
git flow feature start ca12345
```

#### Finalização da branch feature
Quando você concluir o trabalho de desenvolvimento na branch, a próxima etapa é mesclar a ramificação de feature na de develop.

Sem as extensões do git-flow:
```
git checkout develop
git merge feature/12345
```
Usando as extensões do git-flow:
```
git flow feature finish
```

Faça push em Develop para o repositório remoto.
```
git push
```

## Branch de release
Uma vez que a branch **develop** adquiriu recursos o bastante para um lançamento (ou uma data de lançamento predeterminada está se aproximando), você criar uma branch **release** a partir de develop. Criar esta ramificação dá início ao próximo ciclo de lançamento, portanto nenhum novo recurso pode ser adicionado depois deste ponto—apenas correções de bug. Quando estiver pronta para ser lançada, a ramificação de lançamento é mesclada com a branch principal e marcada com uma tag com um número de versão. Além disso, ela deve ser mesclada de volta com a ramificação develop caso for efetuado alguma alteração de bug.

Uma nova branch de release pode ser criada usando os seguintes métodos.
Sem as extensões do git-flow:
```
git checkout develop
git checkout -b release/0.1.0
```

Ao utilizar extensões do git-flow:
```
git flow release start 0.1.0
```
#### finalizar a branch de release
Ao finalizar a branch release, ela será automaticamente removida e efetuada a mesclagem LOCALMENTE para branch Master e Develop, também é criada uma TAG.

Use os seguintes métodos:

Sem as extensões do git-flow:
```
git checkout master
git merge 0.1.0
```
Ou, com a extensão do git-flow:
```
git flow release finish
```

Faça push em Develop, efetue a homologação, envie para Master e log em seguida envie também a tag.
```
git checkout develop
git push

git checkout master
git push

git push origin 0.1.0
```

Faça o envio da tag
```
git checkout 0.1.0
git push origin 0.1.0
```

## Branches de hotfix

As branches de manutenção ou “hotfix” são usadas para corrigir com rapidez lançamentos de produção. As branches de hotfix se parecem com releases e e features, com a diferença de serem baseadas a partir da branch master em vez de develop. 

Uma ramificação de hotfix pode ser criada usando os seguintes métodos:

Sem as extensões do git-flow:
```
git checkout master
git checkout -b hotfix/0.1.0
```
Ao utilizar extensões do git-flow: 
```
git flow hotfix start 0.1.1
```


#### Finalizar a branch hotfix

Assim que a correção é concluída nesta branch, ela automaticamente será mesclada na branch **master**, **develop** e cria uma **tag**.

Sem as extensões do git-flow:
```
git checkout master
git merge hotfix_branch
git checkout develop
git merge hotfix_branch
git branch -D hotfix_branch
```

Ao utilizar extensões do git-flow: 
```
git flow hotfix finish
```

Após mesclagem do Hotfix foi feita para develop, envie para o repositório remoto e faça a homologação.
```
git checkout develop
git push 
```

Em eguida envie para release master.
```
git checkout master
git push
```

Envie a tag para deploy em produção.
```
git push origin 0.1.1
```

# Resumo
Aqui, é discutido o Gitflow Workflow. Um dos muitos estilos de fluxos de trabalho Git que a equipe podem utilizar.
 
O fluxo geral do Gitflow é:

- Uma branch de develop é criada a partir da branch master.
- Uma branch de release é criada a partir da branch develop.
- branches de release são criadas a partir de branch develop.
- Quando uma feature é concluída, ele é mesclado na branch develop. 
- Quando a branch de release é concluída, ela é mesclada nas branches de develop e master.
- Caso um problema seja detectado na branch master,uma branch de hotfix é criada a partir da master.
- Após a conclusão da branch de hotfix, ela é mesclada para as branches de develop e master.


Fontes:

https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow

https://fjorgemota.com/git-flow-uma-forma-legal-de-organizar-repositorios-git/