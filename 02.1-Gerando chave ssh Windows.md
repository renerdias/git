**Opcionalmente instale o aplicativo VisualCode para a construção dos códigos, na etapa de instalação do Git opcionalmente adicionamos o Visual Studio Code como editor padrão.**

1. Baixe a aplicação git para Windows
[https://git-scm.com/download/win](https://git-scm.com/download/win)   
![](imagens/git-win-00.png)

Instale da seguinte forma:   

![](imagens/git-win-01.jpg)
![](imagens/git-win-02.jpg)
![](imagens/git-win-03.jpg)
![](imagens/git-win-04.jpg)
![](imagens/git-win-05.jpg)
![](imagens/git-win-06.jpg)
![](imagens/git-win-07.jpg)
![](imagens/git-win-08.jpg)
![](imagens/git-win-09.jpg)

2. Vá para "Git Bash".
```
ssh-keygen
```

2. Pressione <kbd>Enter</kbd>.

3. Ele solicitará que você salve a chave no diretório específico, no meu caso apenas substitui o nome do arquivo para **gitlab_rsa**
```
ssh-keygen                     
Generating public/private rsa key pair.
Enter file in which to save the key (/c/Users/claudio.antonio/.ssh/gitlab_rsa): /c/Users/claudio.antonio/.ssh/gitlab_rsa
```
4. Pressione <kbd>Enter</kbd>. Ele solicitará que você digite a senha ou digite sem senha. No nosso caso não adicionaremos senha.
```
ssh-keygen                     
Generating public/private rsa key pair.
Enter file in which to save the key (/home/dz/.ssh/id_rsa): /home/dz/.ssh/gitlab_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again:
```

5. A chave pública será criada para o diretório específico.
```
ssh-keygen                     
Generating public/private rsa key pair.
Enter file in which to save the key (/home/dz/.ssh/id_rsa): /home/dz/.ssh/gitlab_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/dz/.ssh/gitlab_rsa.
Your public key has been saved in /home/dz/.ssh/gitlab_rsa.pub.
The key fingerprint is:
SHA256:QoMqPz8tajt1XwvZ/Wf6hCh3RVyqpAQTY71CJAS0/cI dz@pc
The key's randomart image is:
+---[RSA 2048]----+
|   .ooo.Bo      .|
|     + o.+.    o.|
|    o +.  ... . o|
|   . o o...o . . |
|. .   E S+...   .|
| o  . .oo o .. o |
|  o. o . o..o.o .|
|  ooo . . .o ...o|
| .o+.o        .=.|
+----[SHA256]-----+
```

6. Agora vá ao diretório especificado anteriormente.
```
cd /c/Users/claudio.antonio/.ssh/
```

7. Você verá um arquivo gitlab_rsa.pub. Abra-o em um visualizador de texto chamado cat.
```
ls

cat gitlab_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDDYzSYenzKlQtMMQpHDhMUrs9skCffN3RsYAj8ELO+6C+EjKMMs8uuJ+zOWPNWo1DHHWKntls00DV4e84e8bRvxrNe66D+B0LXhkWbD6Y1mAdFtt/8WGgDLmzEAlikmQGQnkacwQcyfSGm8TpUTOMaxlQPx2HrdSILrRyIleSxDkOXjhBK/GnYZLjLjgueecA2GTXrPz9ZKrmmKQDgu3qtadNHtT9/Hxs0P3RiSCZkt+8uZxvji/ez+4HaExids5ffMn35/mITNmAjZsORMHH8RTz5aF7ZH++aNQ/5fE6ktGwreO+IoxEnPCIVrGCboXK3++38SO4QYtcwgSpZGabSdBOYGJKRImHeno3HPltf7xtQoB+AQfM/pvopdPyfflSUBzvoE4X6TCZOe/C+TMtiXuPHRoaTNzl2o2q3RvNZVb6UNB/nqA/HIaTo3h8RSlpUSrJe50f0dlLrDCkZ9m1L4ARc1HbKP/Togt8RHCiH2SLhsT3nREnT29ceyuJt6Rc= claudio.antonio@AN085293-1

```

8. Vá para [https://gitlab.com/profile/keys](https://gitlab.com/profile/keys) .
* Cole aqui no campo de texto "chave".
* Agora clique no "Título" abaixo. Ele será preenchido automaticamente.
* Depois clique em "Adicionar chave".

OBS: Caso você esteja perdido ao copiar o arquivo pub no Windows para a área de transferência. tente o seguinte:
```
type %userprofile%\.ssh\id_rsa.pub | clip
```

9. Para validar sua chave ssh execute ssh-add adicionando o caminho do arquivo privado:
```
eval $(ssh-agent -s)
ssh-add /c/Users/claudio.antonio/.ssh/gitlab_rsa
Identity added: /c/Users/claudio.antonio/.ssh/gitlab_rsa (claudio.antonio@AN085293-1)
```

9. Para testar sua chave GPG execute o comando:
```
ssh -T git@gitlab.com    
Welcome to GitLab, @claudiosilva!
```

Fonte: https://stackoverflow.com/questions/40427498/getting-permission-denied-public-key-on-gitlab    
https://medium.com/test-after-deploy/usando-o-chaves-ssh-no-windows-fa459ee42079