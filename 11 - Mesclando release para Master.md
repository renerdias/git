### Mesclando para master
Esta branch, foi testada, não foi identificado nenhum problema e será mesclada com master, basta seguir o mesmo fluxo anterior de mesclagem, seja por linha de comando ou interface. Neste caso faremos via interface gráfica do GitLab.

Repositório -> Branches -> Merge request **Na branch release-1.1.0**

Vamos mesclar a branch **release-*** com **master**.

Faça uma boa descrição do que mudou, motivos e outros para ficar o máximo de informações para qualquer um técnico ou não saber os motivos.

Na combobox, desmarque a opção **Merge options: Delete source branch when merge request is accepted.**

Clique no botão **Submit merge request**   


Na próxima página, vamos aceitar a mesclagem clicando no botão **merge**.   

Vamos clicar no botão merge.   

Se explorarmos agora a master, nossas mudanças já foram feitas com sucesso inserindo todos os artefatos novos e mesclando alterados já existentes.