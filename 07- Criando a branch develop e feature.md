### Branches
Uma branch no Git é uma linha do tempo do código, é um ponteiro móvel para os commits. 
O nome da primeira branch no Git é a **master**. Como você inicialmente fez commits, você tem uma branch master.   
Cada vez que você faz um commit ele avança automaticamente.

O que acontece se você criar um novo branch? Bem, isso cria uma nova linha do tempo para que você possa desenvolver. 
Vamos criar uma nova branch chamado develop pelo gitlab.   

Na tela inicial do seu GitLab, seu projeto **repotest** criado anteriormente estará listado. Acesse ele.   
Acesse no menu lateral Repositório -> Branches e pressione o botão **Nova Branch**

Adicione a Branch name **develop**   
Create from **master**

![](imagens/branch-01.png)

Temos agora duas Branches que têm vida infinita seguindo orientações do GitFlow.

Vamos criar em seguida uma Branch auxiliar que é a Feature, aquela que é uma linha do tempo para desenvolver uma funcionalidade e têm vida finita.

Agora que já sabemos como criar uma branch pela interface gráfica, vamos criar a Branch feature via linha de comando.    

Vá até o diretório repotest do seu computador, caso tenha excluído, efetue o git clone dele.   
Acesse o repotório e para trazer às mudanças feitas no repositório remoto para o diretório local pode ser executado o comando:
```
git pull
From gitlab.com:claudiosilva/repotest
 * [new branch]      develop    -> origin/develop
Already up to date.
```

Para ver qual branch você está, pode ser executado o comando branch com a flag -v
```
git branch -v 
```

Para verificar todas as branchs deste repositório, pode ser executado o comando branch com a flag -a
```
git branch -a 
```

Podemos mudar de branch facilmente, o comando abaixo sairá da branch que você está (provavelmente a master) para develop:
```
git checkout develop
```

Supondo que queremos mudar a mensagem de rodapé, criaremos uma nova branch chamada **feature/novo-rodape** clonando todo o conteúdo da branch **develop**.

```
git checkout -b feature/novo-rodape develop
```

Faremos a mudança do rodapé no arquivo **index.html** removendo a frase:  
de: "Site de exemplo para praticar versionamento no Git e GitLab"
para: "Mudamos a branch feature/novo-rodape"

Vemos o que foi modificado até agora no código:
```
git diff
```

No início da linha quando iniciar com o sinal de menos (-) e com a cor vermelha é o que foi modificado.
Quando a linha iniciar com sinal de mais (+) indica que está atualmente.
```
       </main>
     <footer class="footer">
-      <p style="text-align: center;">Site de exemplo para praticar versionamento no Git e GitLab</p>
+      <p style="text-align: center;">Mudamos a branch feature/novo-rodape</p>
     </footer>
   </div>
```

Agora precisamos mandar o git monitorar este arquivo alterado executando o comando add:
```
git add *
```

Confirme se o git está monitorando o arquivo:
```
git status
```

Vamos efetuar o commit:
```
git commit -m "Nova versão do rodape"
```

Vamos efetuar o envio da nova branch **feature/novo-rodape** para o GitLab. Se você executar `git push` ele não saberá para onde enviar, porque esta branch foi criada, mas não declarada corretamente, mesmo assim, o Git sendo bastante intuitivo lhe apresentará a linha de comando para sincronizar a nova branch.
```
git push --set-upstream origin feature/novo-rodape
```

O comando de upstream é necessário somente desta vez, pois não havia a branch no repositório remoto. Ao acessarmos a página do repositório no GitLab veremos que a nova branch estará lá em:   
Repositório -> Branches