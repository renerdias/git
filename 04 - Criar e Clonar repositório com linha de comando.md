# Criando um repositório na linha de comando.

Como estamos utilizando uma ferramenta universal que é o Bash nativamente no Linux ou pelo Git Bash os comandos serão os mesmos.

* Crie um diretório chamado testmanual
```
mkdir testmanual
```
* Acesse o diretório
```
cd testmanual
```

Este diretório não têm nada de especial ainda, para isto precisaremos apontar este diretório para um repositório remoto.
```
git init
```

**git init** é um comando único usado durante a configuração inicial de um novo repositório. A execução deste comando vai criar um novo subdiretório *.git* no diretório de trabalho atual. Isso também vai criar uma nova branch master. Para listar o diretório *.git* executamos o comando:
```
ls -a
```

Crie um arquivo de teste:
```
touch teste.txt
```

Peça ao Git para rastrear este novo arquivo dentro do diretório.
```
git add *
```

Efetue o commit:
```
git commit -m "Meu primeiro commit"
```

A saída será que está utilizando a branch master, o usuário root quem fez o commit, sua mensagem de commit, que havia somente um arquivo a ser adicionado ao repositório:
```
git commit -m "Meu primeiro commit"
[master (root-commit) af4ef72] Meu primeiro commit
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 teste.txt

```

Agora é hora de enviar sincronizar as mudanças do seu diretório local para o repositório externo. Lembra que tinhamos criado um grupo e subgrupo anteriormente? pois bem, vamos utilizar o grupo principal para enviar o nosso diretório repotest:    
git remote add origin URL-DO-GRUPO/NOME-DO-REPOSITORIO.git
```
git remote add origin https://gitlab.com/test-treinamento/testmanual.git
```

Agora nós estamos prontos para enviar as mudanças do nosso diretório para o repositório externo:
```
git push
```

Opa, o comando acima ainda não é a parte final, o Git é muito interativo, resumidamente ele diz que como é a primeira vez nesta branch master, você deve fazer o upstream.
```
git push --set-upstream origin master
```

Após isto, a saída do comando deve aparecer conforme abaixo e com toda certeza foi criado o repositório externo no Git estará com suas mudanças.
```
 git push --set-upstream origin master
Username for 'https://gitlab.com': claudiosilva
Password for 'https://claudiosilva@gitlab.com': 
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Writing objects: 100% (3/3), 873 bytes | 873.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
remote: 
remote: 
remote: The private project test-treinamento/testmanual was successfully created.
remote: 
remote: To configure the remote, run:
remote:   git remote add origin https://gitlab.com/test-treinamento/testmanual.git
remote: 
remote: To view the project, visit:
remote:   https://gitlab.com/test-treinamento/testmanual
remote: 
remote: 
To https://gitlab.com/test-treinamento/testmanual.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
```

![](imagens/repo-01.png)
