### Merge request pela interface gráfica.

Aproveitando os recursos do GitLab, podemos fazer facilmente seguindo o seguinte roteiro:

Repositório -> Branches -> Merge request **Na branch feature/rodape-verde**

Vamos mesclar esta branch com a branch develop, clicamos em **Change branches**   
Em **Source Branch** deixe marcado **feature/novo-rodape**.   
Em **Target branch** é para onde vai a mesclagem, neste caso para **develop**   
Clicamos em **Compare branches and continue**

Faça uma boa descrição do que mudou, motivos e outros para ficar o máximo de informações para qualquer um técnico ou não saber os motivos.

Em merge options há um combo box marcado indicando que se esta mesclagem for aceita, esta branch será removida automaticamente. Deixe marcada. 

Após descrever a mudança, Clique no botão **Submit merge request**   

Na próxima página temos as opções de aceitar ou não o merge, também temos diversos recurso como: dar joinha, enviar uma mensagem porquê não foi efetuada a mesclagem, baixar o código ou simplesmente aceitar a mesclagem clicando no botão **merge**.

Vamos clicar no botão merge.