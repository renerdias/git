1. No **Linux** basta executar o comando acima.
```
ssh-keygen
```

2. Pressione <kbd>Enter</kbd>.

3. Ele solicitará que você salve a chave no diretório específico, no meu caso apenas substitui o nome do arquivo para **gitlab_rsa**
```
ssh-keygen                     
Generating public/private rsa key pair.
Enter file in which to save the key (/home/dz/.ssh/id_rsa): /home/dz/.ssh/gitlab_rsa
```
4. Pressione <kbd>Enter</kbd>. Ele solicitará que você digite a senha ou digite sem senha.
```
ssh-keygen                     
Generating public/private rsa key pair.
Enter file in which to save the key (/home/dz/.ssh/id_rsa): /home/dz/.ssh/gitlab_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again:
```

5. A chave pública será criada para o diretório específico.
```
ssh-keygen                     
Generating public/private rsa key pair.
Enter file in which to save the key (/home/dz/.ssh/id_rsa): /home/dz/.ssh/gitlab_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/dz/.ssh/gitlab_rsa.
Your public key has been saved in /home/dz/.ssh/gitlab_rsa.pub.
The key fingerprint is:
SHA256:QoMqPz8tajt1XwvZ/Wf6hCh3RVyqpAQTY71CJAS0/cI dz@pc
The key's randomart image is:
+---[RSA 2048]----+
|   .ooo.Bo      .|
|     + o.+.    o.|
|    o +.  ... . o|
|   . o o...o . . |
|. .   E S+...   .|
| o  . .oo o .. o |
|  o. o . o..o.o .|
|  ooo . . .o ...o|
| .o+.o        .=.|
+----[SHA256]-----+
```

6. Agora vá ao diretório especificado **/home/dz/.ssh**.

7. Você verá um arquivo gitlab_rsa.pub. Abra-o em um editor de texto e copie todo o texto dele.

8. Vá para [https://gitlab.com/profile/keys](https://gitlab.com/profile/keys) .
* Cole aqui no campo de texto "chave".
* Agora clique no "Título" abaixo. Ele será preenchido automaticamente.
* Depois clique em "Adicionar chave".

9. No **Linux** Para validar sua chave ssh execute ssh-add adicionando o caminho do arquivo privado:
```
ssh-add ~/.ssh/gitlab_rsa
Enter passphrase for /home/dz/.ssh/gitlab_rsa: 
Identity added: /home/dz/.ssh/gitlab_rsa (dz@pc)
```

9. Para testar sua chave GPG execute o comando:
```
ssh -T git@gitlab.com    
Welcome to GitLab, @claudiosilva!
```

Fonte: https://stackoverflow.com/questions/40427498/getting-permission-denied-public-key-on-gitlab